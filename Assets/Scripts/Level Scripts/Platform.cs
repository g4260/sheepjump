﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Platform : MonoBehaviour
{
    [SerializeField]
    public Transform spikes;

    [SerializeField]
    private GameObject coin;

    private bool fallDown;

    void Start()
    {
        ActivatePlatform();
    }

    void ActivateSpike()
    {

        spikes.gameObject.SetActive(true);

        spikes.DOLocalMoveY(0.7f, 1.3f).SetLoops(-1, LoopType.Yoyo).SetDelay(Random.Range(3f, 5f));
    }

    void AddCoin()
    {
        GameObject c = Instantiate(coin);
        c.transform.position = transform.position;
        c.transform.SetParent(transform);
        c.transform.DOLocalMoveY(1f, 0f);
    }

    void ActivatePlatform()
    {
        int chance = Random.Range(0, 100);
        if (chance > 70)
        {
            int type = Random.Range(0, 8);
            switch (type)
            {
                case 1:
                    AddCoin();
                    break;
                case 2:
                    fallDown = true;
                    break;
                case 3:
                    ActivateSpike();
                    break;
                case 4:
                    fallDown = true;
                    break;
                case 5:
                    AddCoin();
                    break;
                case 6:
                    fallDown = true;
                    break;
                case 7:
                    AddCoin();
                    break;
                default:
                    ActivateSpike();
                    break;
            }

        }
    }

        void InvokeFalling()
        {
            gameObject.AddComponent<Rigidbody>();
        }

        void OnCollisionEnter(Collision target)
        {
            if(target.gameObject.tag == "Player")
            {
                if (fallDown)
                {
                  fallDown = false;
                   Invoke("InvokeFalling", 2f); //leave this line in the fallDown condition - platforms start to fall off
                }
            }
        }
    
}
